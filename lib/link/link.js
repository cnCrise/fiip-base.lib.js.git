class LinkClient {
  constructor() {
    this.type = '';
    this.status = 0;
    this.address = null;
    this.port = 0;
    this.id = null;
    this.fd = null;
    this.send = null;
    this.recv = null;
  }
}

const Link = {
  Client: LinkClient,
  type: {
    serial: 0x10,
    usart: 0x11,
    tty: 0x12,
    udp: 0x20,
    tcp: 0x21,
    http: 0x30,
    websocket: 0x31,
  },
  status: {
    temp: 0x00,
    fixed: 0x80,
    client: 0x90,
    server: 0xa0,
  },
};
module.exports = Link;
