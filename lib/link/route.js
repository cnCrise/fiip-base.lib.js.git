const Link = require('./link.js');

class Route {
  constructor() {
    this.routes = {}; //{id:Link}
  }

  update(id, link) {
    if (this.routes[id] == null) {
      this.routes[id] = link;
    } else if (this.routes[id].status == Link.status.temp) {
      this.routes[id] = link;
    }
  }
  fix(id) {
    //固定路由(添加后无法修改，除非删除)
    if (this.routes[id] != null) {
      this.routes[id].id = id;
      this.routes[id].status = Link.status.fixed;
    }
  }

  remove(id) {
    delete this.routes[id];
  }

  find(id) {
    if (this.routes[id] != null) {
      return this.routes[id];
    }
    return null;
  }
}

const route = new Route();
module.exports = route;
