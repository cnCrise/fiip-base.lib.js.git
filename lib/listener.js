'use strict';
const listeners = {}; //监听指令

const add = (cmd, fun) => {
  if (listeners[cmd]) {
    console.log('fiip listener %s has been replaced.', cmd.toString(16));
  }
  listeners[cmd] = { cmd, fun };
};
const solve = async (msg, link) => {
  if (listeners[0xffff]) {
    await listeners[0xffff].fun(msg, link);
  }
  if (listeners[(msg.cmd[0] << 8) + msg.cmd[1]] && (msg.cmd[0] << 8) + msg.cmd[1] != 0xffff) {
    await listeners[(msg.cmd[0] << 8) + msg.cmd[1]].fun(msg, link);
  } else {
    if (!listeners[0xffff]) {
      console.log('fiip no listener 0xffff.');
    } else if (msg.cmd[0] != 0 && msg.cmd[1] != 0) {
      console.log('fiip no listener 0x%s.', ((msg.cmd[0] << 8) + msg.cmd[1]).toString(16));
    }
  }
};

const listener = { add, solve };
module.exports = listener;
