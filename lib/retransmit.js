'use strict';
const list = {}; // 重传列表
let retransmitTimes = 0;

const add = (data, dataLen, link) => {
  const index = (data[28 + 0] << 8) + data[28 + 1];

  list[index] = {
    data: Buffer.from(data),
    dataLen: dataLen,
    link: link,
  };
  if (data[25] == 0x00) {
    list[index].times = 5;
    list[index].interval = 1;
  } else {
    list[index].times = data[25] >> 4;
    list[index].interval = data[25] & 0x0f;
  }
};
const transmit = () => {
  retransmitTimes++;

  let retransmit = null;
  for (const key in list) {
    retransmit = list[key];
    if (retransmitTimes % retransmit.interval == 0) {
      const fiip = require('../fiip.js');
      fiip.sendOriginal(retransmit.data, retransmit.dataLen, retransmit.link);
      if (--retransmit.times == 0) {
        delete list[key];
      }
    }
  }
};

const remove = (index) => {
  delete list[index];
};

const retransmit = { add, transmit, remove };
module.exports = retransmit;
