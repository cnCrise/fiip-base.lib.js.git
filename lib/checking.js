'use strict';
const crc = require('./frame/crc.js');
const lists = {}; // 查重列表

const isDuplicate = (msg) => {
  const index = (msg.index[0] << 8) | msg.index[1];
  const cmd = (msg.cmd[0] << 8) | msg.cmd[1];
  const dataCRC = crc.get16(msg.body, (msg.len[0] << 8) | msg.len[1]);
  if (!lists[msg.srcAddr]) {
    lists[msg.srcAddr] = {};
  }
  const list = lists[msg.srcAddr];

  if (list[index + cmd + dataCRC] == dataCRC) {
    return 1;
  }

  list[index + cmd + dataCRC] = dataCRC;
  return 0;
};

const clear = (devAddr) => {
  lists[devAddr] = {};
};

const checking = { isDuplicate, clear };
module.exports = checking;
