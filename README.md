# FIIP

极速万物互联协议(Fast Internet Interconnection Protocol).

## 安装

```bash
npm install fiip --save
```

## 使用

```js
const fiip = require('./fiip');
const txd = new fiip.Format();

const my = { id: Buffer.from('0000000000000000', 'hex'), key: Buffer.from('00000000', 'hex') };
fiip.init();
fiip.setId(my.id, my.key);
txd.setVar('body', Buffer.from('this is body.'), 13);
fiip.request(txd, my.id, null);
```

## 许可证

MIT

## 相关链接

[git](https://gitee.com/fengdid/fiip)
